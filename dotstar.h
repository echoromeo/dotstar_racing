/*
 * dotstar.h
 *
 * Created: 01.09.2018 00:41:28
 * Author: Egil
 */


#ifndef DOTSTAR_H_
#define DOTSTAR_H_

#define LED_SPI				SPI0
#define LED_SPI_PORT		PORTA
#define LED_DATA_PIN        PIN4_bm
#define LED_SCK_PIN         PIN6_bm
#define LED_ALT1			PORTMUX_SPI0_ALT1_gc
#define LED_ALT2			PORTMUX_SPI0_ALT2_gc

#define LED0_USART			 USART0
#define LED0_USART_PORT      PORTA
#define LED0_DATA_PIN        PIN4_bm
#define LED0_SCK_PIN         PIN6_bm
#define LED0_ALT1			 PORTMUX_USART0_ALT1_gc

#define LED1_USART			 USART1
#define LED1_USART_PORT      PORTC
#define LED1_DATA_PIN        PIN4_bm
#define LED1_SCK_PIN         PIN6_bm
#define LED1_ALT1			 PORTMUX_USART1_ALT1_gc

#define LED2_USART			 USART2
#define LED2_USART_PORT      PORTF
#define LED2_DATA_PIN        PIN0_bm
#define LED2_SCK_PIN         PIN2_bm
#define LED2_ALT1			 PORTMUX_USART2_ALT1_gc

#define DOTSTAR_LED_BYTES   (4)

/*
 * The structs you need
 */

typedef __uint24 uint24_t;

typedef union {
	uint8_t array[4];
	struct {
		uint8_t brightness : 5;
		uint8_t ones	   : 3; //Always set to 0x7
		union {
			struct {
				uint8_t b;
				uint8_t g;
				uint8_t r;
				};
				uint24_t channel;
			};
	};
} color_t;

typedef struct {
	uint16_t h;
	uint8_t s;
	uint8_t b;
} hsb_t;


/*
 * Private functions
 */

//static void LED_Send_Bit(uint8_t Data);

/*
 * Public functions
 */

void dotstar_init(void);

void dotstar_write_start(void);
void dotstar_write_end(uint16_t total_length);
void dotstar_write_single_rgb(color_t Color);
void dotstar_write_array_rgb(color_t *array, uint16_t length);
void dotstar_write_constant_rgb(color_t Color, uint16_t length);

void dotstar_configure_single_rgb(color_t Color);
void dotstar_configure_array_rgb(color_t *LED_String, uint16_t LED_string_length);
void dotstar_configure_constant_rgb(color_t Color, uint16_t LED_string_length);
void dotstar_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t LED_string_length);
void dotstar_configure_pong(color_t *endzone1, color_t *endzone2, uint8_t endsize, color_t pong_color, uint8_t pong_position, uint8_t total_size);

color_t hsb2rgb(hsb_t led_array);
color_t rotate_pixel_hue(uint8_t value, uint8_t brightness);

void dotstar0_init(void);
void dotstar0_send_byte(uint8_t Data);

void dotstar0_write_start(void);
void dotstar0_write_end(uint16_t total_length);
void dotstar0_write_single_rgb(color_t Color);
void dotstar0_write_array_rgb(color_t *array, uint16_t length);
void dotstar0_write_constant_rgb(color_t Color, uint16_t length);

void dotstar0_configure_single_rgb(color_t Color);
void dotstar0_configure_array_rgb(color_t *LED_String, uint16_t LED_string_length);
void dotstar0_configure_constant_rgb(color_t Color, uint16_t LED_string_length);
void dotstar0_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t LED_string_length);
void dotstar0_configure_pong(color_t *endzone1, color_t *endzone2, uint8_t endsize, color_t pong_color, uint8_t pong_position, uint8_t total_size);

void dotstar1_init(void);
void dotstar1_send_byte(uint8_t Data);

void dotstar1_write_start(void);
void dotstar1_write_end(uint16_t total_length);
void dotstar1_write_single_rgb(color_t Color);
void dotstar1_write_array_rgb(color_t *array, uint16_t length);
void dotstar1_write_constant_rgb(color_t Color, uint16_t length);

void dotstar1_configure_single_rgb(color_t Color);
void dotstar1_configure_array_rgb(color_t *LED_String, uint16_t LED_string_length);
void dotstar1_configure_constant_rgb(color_t Color, uint16_t LED_string_length);
void dotstar1_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t LED_string_length);
void dotstar1_configure_pong(color_t *endzone1, color_t *endzone2, uint8_t endsize, color_t pong_color, uint8_t pong_position, uint8_t total_size);

void dotstar2_init(void);
void dotstar2_send_byte(uint8_t Data);

void dotstar2_write_start(void);
void dotstar2_write_end(uint16_t total_length);
void dotstar2_write_single_rgb(color_t Color);
void dotstar2_write_array_rgb(color_t *array, uint16_t length);
void dotstar2_write_constant_rgb(color_t Color, uint16_t length);

void dotstar2_configure_single_rgb(color_t Color);
void dotstar2_configure_array_rgb(color_t *LED_String, uint16_t LED_string_length);
void dotstar2_configure_constant_rgb(color_t Color, uint16_t LED_string_length);
void dotstar2_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t LED_string_length);
void dotstar2_configure_pong(color_t *endzone1, color_t *endzone2, uint8_t endsize, color_t pong_color, uint8_t pong_position, uint8_t total_size);

#endif /* DOTSTAR_H_ */

