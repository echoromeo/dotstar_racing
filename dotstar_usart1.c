/*
 * dotstar.c
 *
 * Created: 01.09.2018 00:40:57
 * Author: Egil
 */

#include <avr/io.h>
#include <stdint.h>
#include "dotstar.h"

/*
 * Local defines
 */


/*
 * Private functions
 */


/*
 * Public functions
 */

void dotstar1_init(void)
{
	LED1_USART.CTRLB = USART_TXEN_bm;
	LED1_USART.CTRLC = USART_CMODE_MSPI_gc;
//	LED1_USART.BAUD = USART_BAUD_RATE(5000000);
	LED1_USART.BAUD = 64*2; //64 is minimum, but that did not work for 900 LEDs

#if (LED1_DATA_PIN == PIN4_bm) //Alternate
	PORTMUX.USARTROUTEA |= LED1_ALT1;
#endif

	// Set MOSI and SCK to output
	LED1_USART_PORT.OUTCLR = LED1_DATA_PIN | LED1_SCK_PIN;
	LED1_USART_PORT.DIRSET = LED1_DATA_PIN | LED1_SCK_PIN;
}

void dotstar1_send_byte(uint8_t Data)
{
	while(!(LED1_USART.STATUS & USART_DREIF_bm)) {
		// Wait for empty buffer
	}

	LED1_USART.TXDATAL = Data;
}

void dotstar1_write_start(void) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES; Byte_Count > 0; Byte_Count--) {
		dotstar1_send_byte(0x00);
	}	
}

void dotstar1_write_end(uint16_t total_length) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES+(total_length/16)+1; Byte_Count > 0; Byte_Count--) {
		dotstar1_send_byte(0x00);
	}

	while(!(LED1_USART.STATUS & USART_TXCIF_bm));
	LED1_USART.STATUS = USART_TXCIF_bm;
}

void dotstar1_write_single_rgb(color_t Color)
{
	for(uint8_t Byte_Count = 0; Byte_Count < DOTSTAR_LED_BYTES; Byte_Count++) {
		dotstar1_send_byte(Color.array[Byte_Count]);
	}
}

void dotstar1_write_array_rgb(color_t *array, uint16_t length)
{
	uint8_t *LED_Bytes = (uint8_t *) array;
	for(uint16_t Byte_Count = 0; Byte_Count < length*DOTSTAR_LED_BYTES; Byte_Count++) {
		dotstar1_send_byte(LED_Bytes[Byte_Count]);
	}
}

void dotstar1_write_constant_rgb(color_t Color, uint16_t length)
{
	while(length--) {
		dotstar1_write_single_rgb(Color);
	}
}

void dotstar1_configure_single_rgb(color_t Color)
{
	dotstar1_write_start();
	dotstar1_write_single_rgb(Color);
	dotstar1_write_end(0);
}

void dotstar1_configure_array_rgb(color_t *LED_String, uint16_t LED_string_length)
{
	dotstar1_write_start();
	dotstar1_write_array_rgb(LED_String, LED_string_length);
	dotstar1_write_end(LED_string_length);
}

void dotstar1_configure_constant_rgb(color_t Color, uint16_t LED_string_length)
{
	dotstar1_write_start();
	dotstar1_write_constant_rgb(Color, LED_string_length);
	dotstar1_write_end(LED_string_length);
}

void dotstar1_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t LED_string_length) {
	dotstar1_write_start();
	
	dotstar1_write_constant_rgb(constant, single_position);
	dotstar1_write_single_rgb(single);
	dotstar1_write_constant_rgb(constant, LED_string_length-single_position);

	dotstar1_write_end(LED_string_length);
}

void dotstar1_configure_pong(color_t *endzone1, color_t *endzone2, uint8_t endsize, color_t pong_color, uint8_t pong_position, uint8_t total_size) {
	color_t off = {.brightness = 0x1f};
	
	dotstar1_write_start();
	dotstar1_write_array_rgb(endzone1, endsize); // first 10 LEDs from array
	
	// middle LEDs either all black or with the pong somewhere
	if ((pong_position < endsize) || (pong_position >= total_size-endsize))
	{
		dotstar1_write_constant_rgb(off, total_size-(endsize*2));
	} else {
		dotstar1_write_constant_rgb(off, pong_position-endsize);
		dotstar1_write_single_rgb(pong_color);
		dotstar1_write_constant_rgb(off, total_size-endsize-pong_position-1);		
	}
	
	dotstar1_write_array_rgb(endzone2, endsize); // last 10 LEDs from array
	dotstar1_write_end(total_size);	
}