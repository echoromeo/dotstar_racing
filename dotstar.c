/*
 * dotstar.c
 *
 * Created: 01.09.2018 00:40:57
 * Author: Egil
 */

#include <avr/io.h>
#include <stdint.h>
#include "dotstar.h"

/*
 * Local defines
 */


/*
 * Private functions
 */

static void dotstar_send_byte(uint8_t Data)
{
	while(!(LED_SPI.INTFLAGS & SPI_DREIF_bm)) {
		// Wait for empty buffer
	}

	LED_SPI.DATA = Data;
}

/*
 * Public functions
 */

void dotstar_init(void)
{
	LED_SPI.CTRLB = SPI_BUFEN_bm | SPI_SSD_bm | SPI_MODE_0_gc;
	LED_SPI.CTRLA = SPI_MASTER_bm | SPI_PRESC_DIV4_gc | SPI_ENABLE_bm; // | SPI_CLK2X_bm; 

// #if (LED_DATA_PORT == PORTC) //Alternate 1
// 	PORTMUX.TWISPIROUTEA |= LED_ALT1;
// #elif (LED_DATA_PORT == PORTE) //Alternate 2
// 	PORTMUX.TWISPIROUTEA |= LED_ALT2;
// #endif

	// Set MOSI and SCK to output
	LED_SPI_PORT.OUTCLR = LED_DATA_PIN | LED_SCK_PIN;
	LED_SPI_PORT.DIRSET = LED_DATA_PIN | LED_SCK_PIN;
}

void dotstar_write_start(void) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES; Byte_Count > 0; Byte_Count--) {
		dotstar_send_byte(0x00);
	}	
}

void dotstar_write_end(uint16_t total_length) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES+(total_length/16)+1; Byte_Count > 0; Byte_Count--) {
		dotstar_send_byte(0x00);
	}

	while(!(LED_SPI.INTFLAGS & SPI_TXCIF_bm));
	LED_SPI.INTFLAGS = SPI_TXCIF_bm;
}

void dotstar_write_single_rgb(color_t Color)
{
	for(uint8_t Byte_Count = 0; Byte_Count < DOTSTAR_LED_BYTES; Byte_Count++) {
		dotstar_send_byte(Color.array[Byte_Count]);
	}
}

void dotstar_write_array_rgb(color_t *array, uint16_t length)
{
	uint8_t *LED_Bytes = (uint8_t *) array;
	for(uint16_t Byte_Count = 0; Byte_Count < length*DOTSTAR_LED_BYTES; Byte_Count++) {
		dotstar_send_byte(LED_Bytes[Byte_Count]);
	}
}

void dotstar_write_constant_rgb(color_t Color, uint16_t length)
{
	while(length--) {
		dotstar_write_single_rgb(Color);
	}
}

void dotstar_configure_single_rgb(color_t Color)
{
	dotstar_write_start();
	dotstar_write_single_rgb(Color);
	dotstar_write_end(0);
}

void dotstar_configure_array_rgb(color_t *LED_String, uint16_t LED_string_length)
{
	dotstar_write_start();
	dotstar_write_array_rgb(LED_String, LED_string_length);
	dotstar_write_end(LED_string_length);
}

void dotstar_configure_constant_rgb(color_t Color, uint16_t LED_string_length)
{
	dotstar_write_start();
	dotstar_write_constant_rgb(Color, LED_string_length);
	dotstar_write_end(LED_string_length);
}

void dotstar_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t LED_string_length) {
	dotstar_write_start();
	
	dotstar_write_constant_rgb(constant, single_position);
	dotstar_write_single_rgb(single);
	dotstar_write_constant_rgb(constant, LED_string_length-single_position);

	dotstar_write_end(LED_string_length);
}

void dotstar_configure_pong(color_t *endzone1, color_t *endzone2, uint8_t endsize, color_t pong_color, uint8_t pong_position, uint8_t total_size) {
	color_t off = {.brightness = 0x1f};
	
	dotstar_write_start();
	dotstar_write_array_rgb(endzone1, endsize); // first 10 LEDs from array
	
	// middle LEDs either all black or with the pong somewhere
	if ((pong_position < endsize) || (pong_position >= total_size-endsize))
	{
		dotstar_write_constant_rgb(off, total_size-(endsize*2));
	} else {
		dotstar_write_constant_rgb(off, pong_position-endsize);
		dotstar_write_single_rgb(pong_color);
		dotstar_write_constant_rgb(off, total_size-endsize-pong_position-1);		
	}
	
	dotstar_write_array_rgb(endzone2, endsize); // last 10 LEDs from array
	dotstar_write_end(total_size);	
}

/*
 * Converts HSB (Hue, Saturation, Brightness) values
 * to their corresponding 8-bit RGB values and returns
 * the results as a color_t
 *
 * Legal ranges:
 * h - 0..1535
 * s - 0..255
 * b - 0..255
 *
 * NOTE:
 * Optimized for speed
 * No rounding
 * b is saturated at 254 to avoid
 */
color_t hsb2rgb(hsb_t led_array)
{
	uint16_t h = led_array.h;
	uint8_t s = led_array.s;
	uint8_t b = led_array.b;

	uint8_t c = (b * s) / 256;
	uint8_t m = b >= c ? b - c : 0;
	uint8_t x = (h % 256) * c / 256;
	uint8_t sector = h >> 8;

	if (sector == 0) return (color_t) {.ones=0x7, .brightness=0x1F, .r=c+m   , .g=x+m   , .b=0+m   };
	if (sector == 1) return (color_t) {.ones=0x7, .brightness=0x1F, .r=c-x+m , .g=c+m   , .b=0+m   };
	if (sector == 2) return (color_t) {.ones=0x7, .brightness=0x1F, .r=0+m   , .g=c+m   , .b=x+m   };
	if (sector == 3) return (color_t) {.ones=0x7, .brightness=0x1F, .r=0+m   , .g=c-x+m , .b=c+m   };
	if (sector == 4) return (color_t) {.ones=0x7, .brightness=0x1F, .r=x+m   , .g=0+m   , .b=c+m   };
	if (sector == 5) return (color_t) {.ones=0x7, .brightness=0x1F, .r=c+m   , .g=0+m   , .b=c-x+m };
	return (color_t) {.ones=0x7, .brightness=0x1F};
}

color_t rotate_pixel_hue(uint8_t value, uint8_t brightness)
{
	static uint16_t pong_hue = 0;
	pong_hue += value;
	if (pong_hue >= 1536)
	{
		pong_hue -= 1536;
	}
	return hsb2rgb((hsb_t) {.h=pong_hue, .s=0xff, .b=brightness});
}