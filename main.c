/*
 * main.c
 *
 * Created: 25.09.2018 19:07:35
 * Author : Egil
 */

/*
 * Include files
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include <stdbool.h>
#include <util/atomic.h>
#include "dotstar.h"

//#define TRACK_LEDS_PER_M		60

#define TRACK_TOTAL_LENGHT_LEDS	(300*9) // 45m
#define TRACK_LANES				3 // 900 LEDs per lane
#define TRACK_LANE_LENGTH_LEDS	(TRACK_TOTAL_LENGHT_LEDS / TRACK_LANES)	

#define SPEED_REFRESH_RATE 48
#define SPEED_RTC_VALUE (32768 / SPEED_REFRESH_RATE)
#define SPEED_CNT_1SEC SPEED_REFRESH_RATE

#define RACE_PLAYERS				3
#define RACE_ELEMENTS				10
#define RACE_LAPS					3 // Actual number of laps is one less because of start position?
#define RACE_GHOSTMODE				false
#define RACE_PLAYER_LENGHT			11
#define RACE_PLAYER_START_LENGHT	4
#define RACE_SPEED_DIVIDER			9
#define RACE_SPEED_INCREMENT		4
#define RACE_SPEED_MAX				((RACE_PLAYER_LENGHT - 1) * RACE_SPEED_DIVIDER)/3

#define RACE_INIT_COUNTDOWN (7 * SPEED_CNT_1SEC)
#define RACE_START_COUNTDOWN (3 * SPEED_CNT_1SEC)
#define RACE_FINISH_COUNTDOWN (20 * SPEED_CNT_1SEC)
#define RACE_LANE_DEBOUNCE 8

#if (RACE_GHOSTMODE == true) && (TRACK_LANES < 2)
	#warning "Must have ghostmode with only one lane! Setting to false"
	#undef RACE_GHOSTMODE
	#define RACE_GHOSTMODE false
#endif

#ifdef BUTTONS_XPRO // Testing with three buttons on oled1_xpro

#define PLAYER1PORT				PORTC
#define PLAYER1PINCTRL			PIN2CTRL
#define PLAYER1_bm				PIN2_bm
#define PLAYER1_vect			PORTC_PORT_vect

#warning "Player 2 not on async pin!"
#define PLAYER2PORT				PORTD
#define PLAYER2PINCTRL			PIN1CTRL
#define PLAYER2_bm				PIN1_bm
#define PLAYER2_vect			PORTD_PORT_vect

#define PLAYER3PORT				PORTD
#define PLAYER3PINCTRL			PIN2CTRL
#define PLAYER3_bm				PIN2_bm
#define PLAYER3_vect			PORTD_PORT_vect

#else

// Three different interrupts makes it more fair? Needs to be on fully async pins!
#define PLAYER1PORT				PORTA
#define PLAYER1PINCTRL			PIN2CTRL
#define PLAYER1_bm				PIN2_bm
#define PLAYER1_vect			PORTA_PORT_vect

#define PLAYER2PORT				PORTB
#define PLAYER2PINCTRL			PIN2CTRL
#define PLAYER2_bm				PIN2_bm
#define PLAYER2_vect			PORTB_PORT_vect

#define PLAYER3PORT				PORTC
#define PLAYER3PINCTRL			PIN2CTRL
#define PLAYER3_bm				PIN2_bm
#define PLAYER3_vect			PORTC_PORT_vect

// Three pins in one ISR/PORT for the lane change buttons
#define LANEPORT				PORTD
#define PLAYER1LANEPINCTRL		PIN3CTRL
#define PLAYER1LANE_bm			PIN3_bm
#define PLAYER2LANEPINCTRL		PIN4CTRL
#define PLAYER2LANE_bm			PIN4_bm
#define PLAYER3LANEPINCTRL		PIN5CTRL
#define PLAYER3LANE_bm			PIN5_bm
#define LANE_vect				PORTD_PORT_vect

#endif

//TODO: Probably a better way to define the colors?
#define BRIGHTNESS 0x10
#define INTENSITY 0xff

const color_t BLACK = {.ones=0x7, .brightness = BRIGHTNESS};
const color_t RED = {.ones=0x7, .brightness = BRIGHTNESS, .r = INTENSITY}; // Hue = 0 deg
const color_t YELLOW = {.ones=0x7, .brightness = BRIGHTNESS, .r = INTENSITY, .g = INTENSITY}; // Hue = 60 deg
const color_t GREEN = {.ones=0x7, .brightness = BRIGHTNESS, .g = INTENSITY}; // Hue = 120 deg
const color_t CYAN = {.ones=0x7, .brightness = BRIGHTNESS, .g = INTENSITY, .b = INTENSITY}; // Hue = 180 deg
const color_t BLUE = {.ones=0x7, .brightness = BRIGHTNESS, .b = INTENSITY}; // Hue = 240 deg
const color_t PURPLE = {.ones=0x7, .brightness = BRIGHTNESS, .b = INTENSITY, .r = INTENSITY}; // Hue = 300 deg

const color_t RED_MAX = {.ones=0x7, .brightness = 0x1f, .r = 0xff};
const color_t GREEN_MAX = {.ones=0x7, .brightness = 0x1f, .g = 0xff};
const color_t BLUE_MAX = {.ones=0x7, .brightness = 0x1f, .r = 0xff};

#define PLAYER1_COLOR YELLOW
#define PLAYER2_COLOR CYAN
#define PLAYER3_COLOR PURPLE
#define BLOCK_COLOR	  RED
#define BOOST_COLOR	  GREEN

typedef enum game_stages {
	STAGE_STOPPED,
	STAGE_INIT,
	STAGE_COUNTDOWN,
	STAGE_RACE,
	STAGE_FINISHED,
	STAGE_RESET,
} game_stages_t;

typedef union element_types {
	uint8_t byte;
	uint8_t players			:3;
	struct {
		uint8_t player1		:1;
		uint8_t player2		:1;
		uint8_t player3		:1;
		uint8_t block		:1;
		uint8_t boost		:1;
		uint8_t start		:1;
		uint8_t reserved	:2;
	};
} element_types_t;

typedef struct element {
	uint8_t active;		/* Is the element in the game? */
	uint8_t lane;		/* Lane position */
	uint16_t position;	/* Head. Between 0 and TRACK_LENGHT_LEDS-1, wraps */
	uint8_t lenght;		
	element_types_t type;		/* Block, boost, weapon? */
	union {
		uint32_t attributes;
		uint8_t attribute[4]; 
	};
} element_t;

typedef struct racer {
	uint8_t active;			/* Is the racer in the game? */
	uint8_t lane;			/* Lane position */
	uint16_t position;		/* Head. Between 0 and TRACK_LENGHT_LEDS-1, wraps */
	uint8_t lenght;

	uint8_t lap;
	uint8_t fuel;			/* Also used to determine length? Decrement for every click, refill at position 0? */
	uint8_t speed;			
	uint8_t podium;
} racer_t;



/*
 * Global variables
 */
unsigned long random_seed = 0;
volatile uint8_t refresh = false;
volatile uint8_t lane_debounce[RACE_PLAYERS] = {RACE_LANE_DEBOUNCE, RACE_LANE_DEBOUNCE, RACE_LANE_DEBOUNCE};
	
volatile racer_t racers[RACE_PLAYERS] = {0};
volatile element_t elements[RACE_ELEMENTS] = {0};
volatile element_types_t led_strip[TRACK_LANES][TRACK_LANE_LENGTH_LEDS] = {0};

volatile game_stages_t game_stage = STAGE_RESET;
volatile uint8_t active_racers = 0;

uint16_t fun_hue[3] = {0, 600, 1200};
uint16_t fun_position[3] = {0};
uint8_t fun1_speed = 10;
int8_t fun1_dir = 1;
	
/*
 * functions
 */
void reset_led_strip(void);

uint8_t init_handler(void);
uint8_t countdown_handler(void);
uint8_t race_handler(void);
uint8_t finish_handler(void);

void dotstar_x3_write_start(void);
void dotstar_x3_write_end(uint16_t total_length);
void dotstar0_write_pixel_color(element_types_t type);
void dotstar1_write_pixel_color(element_types_t type);
void dotstar2_write_pixel_color(element_types_t type);
void dotstar_display_race(void);

void handle_speed_isr(uint8_t button);
void handle_lane_isr(uint8_t button);

/*
 * main()
 */
int main(void)
{
	// Configure clock
#if ((F_CPU == 20000000ul) || (F_CPU == 16000000ul))
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, 0);
#elif ((F_CPU == 10000000ul) || (F_CPU == 8000000ul))
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_PDIV_2X_gc | CLKCTRL_PEN_bm);
#endif

	// Init buttons, interrupt
	PLAYER1PORT.PLAYER1PINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	PLAYER2PORT.PLAYER2PINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	PLAYER3PORT.PLAYER3PINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	PLAYER1PORT.INTFLAGS = PLAYER1_bm;
	PLAYER2PORT.INTFLAGS = PLAYER2_bm;
	PLAYER3PORT.INTFLAGS = PLAYER3_bm;

#ifndef BUTTONS_XPRO
	LANEPORT.PLAYER1LANEPINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	LANEPORT.PLAYER2LANEPINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	LANEPORT.PLAYER3LANEPINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	LANEPORT.INTFLAGS = PLAYER1LANE_bm;
	LANEPORT.INTFLAGS = PLAYER2LANE_bm;
	LANEPORT.INTFLAGS = PLAYER3LANE_bm;
#endif

	// Configure RTC to wake up device after sleep
	while (RTC.STATUS);
	RTC.INTCTRL = RTC_OVF_bm;
	RTC.PER = SPEED_RTC_VALUE;
	RTC.CMP = SPEED_RTC_VALUE + 1;
	RTC.CLKSEL = RTC_CLKSEL_INT32K_gc;
	RTC.CTRLA = RTC_RTCEN_bm | RTC_PRESCALER_DIV1_gc | RTC_RUNSTDBY_bm;
	set_sleep_mode(SLEEP_MODE_IDLE); // SLEEP_MODE_STANDBY?
	
	CPUINT.CTRLA = CPUINT_LVL0RR_bm; //Will round-robin make it more fair?
	CPUINT.LVL1VEC = RTC_CNT_vect_num; // Not needed, but moves the RTC out of the round-robin?
	

#ifdef DEBUG
	PORTF.DIRSET = PIN5_bm | PIN3_bm; //LED0 and DBG1
#endif
	
	sei();
	sleep_mode();

	// Init LEDs to black
	dotstar0_init();
	dotstar1_init();
	dotstar2_init();
	sleep_mode();
	dotstar0_configure_constant_rgb(BLACK, TRACK_LANE_LENGTH_LEDS);
	dotstar1_configure_constant_rgb(BLACK, TRACK_LANE_LENGTH_LEDS);
	dotstar2_configure_constant_rgb(BLACK, TRACK_LANE_LENGTH_LEDS);

	while(1) {

		// Go to sleep, wake up on ISR
		sleep_mode();
		
		random_seed++; // Increment until used
		if (game_stage == STAGE_STOPPED) {

			// Do something fun here!
			
			if (refresh)
			{
				refresh = false;

				dotstar0_write_start();
				dotstar1_write_start();
				dotstar2_write_start();

				for (uint16_t i = 0; i < TRACK_LANE_LENGTH_LEDS; i++)
				{
					if (((i > fun_position[0]) && (i < fun_position[0] + 10)) || // Normal position of racer
						((fun_position[0] + 10 >= TRACK_LANE_LENGTH_LEDS) && (i < fun_position[0] + 10 - TRACK_LANE_LENGTH_LEDS))) // Overflow on racer
					{
						dotstar0_write_single_rgb(hsb2rgb((hsb_t) {.h=fun_hue[0], .s=0xff, .b=0xff}));
					} else {
						dotstar0_write_single_rgb(BLACK);
					}
					if (((i > fun_position[1]) && (i < fun_position[1] + 10)) ||
						((fun_position[1] + 10 >= TRACK_LANE_LENGTH_LEDS) && (i < fun_position[1] + 10 - TRACK_LANE_LENGTH_LEDS)))
					{
						dotstar1_write_single_rgb(hsb2rgb((hsb_t) {.h=fun_hue[1], .s=0xff, .b=0xff}));
					} else {
						dotstar1_write_single_rgb(BLACK);
					}
					if (((i > fun_position[2]) && (i < fun_position[2] + 10)) ||
						((fun_position[2] + 10 >= TRACK_LANE_LENGTH_LEDS) && (i < fun_position[2] + 10 - TRACK_LANE_LENGTH_LEDS)))
					{
						dotstar2_write_single_rgb(hsb2rgb((hsb_t) {.h=fun_hue[2], .s=0xff, .b=0xff}));
					} else {
						dotstar2_write_single_rgb(BLACK);
					}
				}
				
				dotstar0_write_end(TRACK_LANE_LENGTH_LEDS);				
				dotstar1_write_end(TRACK_LANE_LENGTH_LEDS);
				dotstar2_write_end(TRACK_LANE_LENGTH_LEDS);
				
				//Move racers
				fun_position[1] += 2;
				fun_position[0] += fun1_speed/8;
				fun_position[2] -= (80 - fun1_speed)/5;

				fun1_speed += fun1_dir;
				if (fun1_speed >= 69)
				{
					fun1_dir = -1;
				} else if (fun1_speed <= 8) {
					fun1_dir = 1;
				}
				
				for (uint8_t i = 0; i < 3; i ++) {
					if (fun_position[i] >= 0xf000) //Wrap around
					{
						fun_position[i] += TRACK_LANE_LENGTH_LEDS;					
					}
					else if (fun_position[i] >= TRACK_LANE_LENGTH_LEDS) //Overflowish
					{
						fun_position[i] -= TRACK_LANE_LENGTH_LEDS;
					}
				}
								
				//Rotate hues	
				fun_hue[0] += 1;
				fun_hue[1] += 7;
				fun_hue[2] += 4;

				for (uint8_t i = 0; i < 3; i ++) {
					if (fun_hue[i] >= 1536)
					{
						fun_hue[i] -= 1536;
					}					
				}
			}

		} else if (refresh) {

#ifdef DEBUG
			PORTF.OUTTGL = PIN5_bm; //LED0
#endif
			refresh = false;
			switch (game_stage)
			{
			case STAGE_INIT:
				game_stage = init_handler();
				break;
			case STAGE_COUNTDOWN:
				game_stage = countdown_handler();
				break;
			case STAGE_RACE:

#ifdef DEBUG
				PORTF.OUTSET = PIN3_bm; //Debug timing
#endif

				game_stage = race_handler();
				break;
		
			case STAGE_FINISHED:
				game_stage = finish_handler();
				break;
			case STAGE_RESET:
				// Reset Racers
				for (uint8_t i = 0; i < RACE_PLAYERS; i++)
				{
					racers[i].active = false;
					racers[i].position = RACE_PLAYER_START_LENGHT-1;
					racers[i].lenght = RACE_PLAYER_START_LENGHT;
#if (TRACK_LANES == 3)
					racers[i].lane = i;
#else
					racers[i].lane = TRACK_LANES-1;
#endif
					racers[i].lap = 0;
					racers[i].speed = 0;
					racers[i].podium = 0;
				}
				
				// Reset Elements
				for(uint8_t i = 0; i < RACE_ELEMENTS; i++) {
					elements[i].active = false;
					elements[i].type.byte = 0;
				}
				
				reset_led_strip();
				
				// Reset to STOPPED
				game_stage = STAGE_STOPPED;
				break;
			default: //STAGE_STOPPED
				break;
			}
		
			// Update LEDs
			dotstar_display_race();

#ifdef DEBUG
			PORTF.OUTCLR = PIN3_bm; //Debug timing
#endif
		}
	}
}

void reset_led_strip(void)
{
	for (uint16_t i = 0; i < TRACK_LANE_LENGTH_LEDS; i++)
	{
		led_strip[0][i].byte = 0;
		led_strip[1][i].byte = 0;
		led_strip[2][i].byte = 0;
	}
}

uint8_t init_handler(void)
{
	static volatile uint16_t opt_in_timer = RACE_INIT_COUNTDOWN;
	
	// Count active racers
	uint8_t prev_cnt = active_racers;
	active_racers = 0;
	for (uint8_t i = 0; i < RACE_PLAYERS; i++)
	{
		if (racers[i].active)
		{
			active_racers++;
			
			// Visual confirmation
			for (uint8_t j = 0; j < TRACK_LANES; j++)
			{
				uint16_t position = i*RACE_PLAYER_LENGHT + (RACE_PLAYERS*RACE_PLAYER_LENGHT)/2 - ((RACE_PLAYERS-1)*RACE_PLAYER_LENGHT);
				for (uint8_t lenght = 0; lenght < RACE_PLAYER_LENGHT; lenght++)
				{
					if (position > TRACK_LANE_LENGTH_LEDS) // Wrap around
					{
						position += TRACK_LANE_LENGTH_LEDS;
					}
					led_strip[j][position--].players |= (1 << i);
				}
			}
		}
	}
	
	if (active_racers > prev_cnt)
	{
		if (active_racers == RACE_PLAYERS)
		{
			// Everyone ready, reduce waiting
			opt_in_timer = SPEED_CNT_1SEC;
		} else {
			// Reset countdown to allow next player to join
			opt_in_timer = RACE_INIT_COUNTDOWN;			
		}
	}

	if (!opt_in_timer--)
	{
		// Reset for next init
		opt_in_timer = RACE_INIT_COUNTDOWN;

		// led_strip should be empty, but probably good to have anyways
		reset_led_strip();

		// Random generating elements
		//TODO: Have minimum one boost and one block?
		srand(random_seed);
		uint8_t num_blocks = 0;
		uint8_t block_lane = 0;
		uint8_t num_boosts = 0;
		for(uint8_t i = 0; i < RACE_ELEMENTS; i++) {
			uint32_t random_val = rand();
			if (random_val & 0x03)
			{
				if (num_boosts < 4)
				{
					num_boosts++;
					elements[i].type.boost = true;
					elements[i].active = true;
					elements[i].lane = (random_val >> 2) % TRACK_LANES;
				}
			} else {
				if (num_blocks < 4)
				{
					num_blocks++;
					elements[i].type.block = true;
					elements[i].active = true;
					
					// Try to distribute at least one block on each lane
					if (block_lane == 0b110)
					{
						elements[i].lane = 0;
						block_lane = 0;
					}
					else if (block_lane == 0b101)
					{
						elements[i].lane = 1;
						block_lane = 0;
					}
					else if (block_lane == 0b011)
					{
						elements[i].lane = 2;
						block_lane = 0;
					}
					else
					{
						elements[i].lane = (random_val >> 4) % TRACK_LANES;
						block_lane |= 1 << elements[i].lane;
					}
				}
			}

			elements[i].position = (random_val % (TRACK_LANE_LENGTH_LEDS/RACE_ELEMENTS)) + (TRACK_LANE_LENGTH_LEDS/RACE_ELEMENTS) * i + RACE_PLAYER_LENGHT*2;
			if (elements[i].position > TRACK_LANE_LENGTH_LEDS) // Wrap around
			{
				elements[i].position -= RACE_PLAYER_LENGHT*3;
			}
			elements[i].lenght = RACE_PLAYER_LENGHT+1;
		}

		// Place race elements!
		for(uint8_t i = 0; i < RACE_ELEMENTS; i++) {
			if(elements[i].active) {
				uint16_t position = elements[i].position;
				for (uint8_t j = 0; j < elements[i].lenght; j++)
				{
					if (position > TRACK_LANE_LENGTH_LEDS) // Wrap around
					{
						position += TRACK_LANE_LENGTH_LEDS;
					}
					led_strip[elements[i].lane][position--].byte |= elements[i].type.byte;			
				}
			}
		}

		return STAGE_COUNTDOWN;
	}
	
	return STAGE_INIT;
}

uint8_t countdown_handler(void)
{
	static volatile uint16_t countdown_timer = RACE_START_COUNTDOWN;

	//Lazy way of clearing start
	for (uint16_t i = 0; i < TRACK_LANE_LENGTH_LEDS; i++)
	{
		led_strip[0][i].start = 0;
		led_strip[1][i].start = 0;
		led_strip[2][i].start = 0;
	}

	//Visual countdown
	uint8_t count = (countdown_timer / SPEED_CNT_1SEC);
	for (uint8_t j = 0; j < TRACK_LANES; j++)
	{
		uint16_t position = count + RACE_PLAYER_START_LENGHT - 1;
		for (uint8_t lenght = 0; lenght < count*2+1; lenght++)
		{
			if (position > TRACK_LANE_LENGTH_LEDS) // Wrap around
			{
				position += TRACK_LANE_LENGTH_LEDS;
			}
			led_strip[j][position--].start = 1;
		}
	}

	if (!countdown_timer--)
	{
		//Lazy way of clearing start
		for (uint16_t i = 0; i < TRACK_LANE_LENGTH_LEDS; i++)
		{
			led_strip[0][i].start = 0;
			led_strip[1][i].start = 0;
			led_strip[2][i].start = 0;
		}

		countdown_timer = RACE_START_COUNTDOWN;
		return STAGE_RACE;
	}
	
	return STAGE_COUNTDOWN;
}

uint8_t race_handler(void)
{
	game_stages_t next_stage = STAGE_RACE;
	static uint8_t finished = 0;
	static uint8_t podium_cnt = 1;

	// Move to function
	for (uint8_t i = 0; i < RACE_PLAYERS; i++)
	{
		// Stand still until first click, else minimum speed is 1.
		if (racers[i].speed > 1)
		{
			if (--racers[i].speed > RACE_SPEED_MAX)
			{
				racers[i].speed = RACE_SPEED_MAX;
			}
			racers[i].position += (racers[i].speed / RACE_SPEED_DIVIDER) + 1;
			
			if (racers[i].lenght < RACE_PLAYER_LENGHT)
			{
				racers[i].lenght++;
			}
		} else {
			racers[i].position += racers[i].speed;
		}
		
		// Handle laps
		if (racers[i].position >= TRACK_LANE_LENGTH_LEDS) {
			// Hit the finish line or not?
			if (racers[i].lap >= RACE_LAPS)
			{
				racers[i].active = false;
				racers[i].speed = 0;
				racers[i].position = TRACK_LANE_LENGTH_LEDS-1;
				racers[i].podium = podium_cnt++;
				//TODO: Add to scoreboard/podium/etc.
				
				//TODO: Start unfinished countdown? Not needed when minimum speed is 1?

				finished++;
			} else {
				racers[i].position -= TRACK_LANE_LENGTH_LEDS;
				racers[i].lap++;
			}
		}
			
			//TODO: Handle not switching lane until entire racer has passed obstacle?
			
		// Handle what hitting elements
		for (uint8_t j = 0; j < RACE_ELEMENTS; j++)
		{
			if (elements[j].active && (elements[j].lane == racers[i].lane))
			{
				uint16_t zone = elements[j].position - racers[i].position;
				if (zone < elements[j].lenght)
				{
					if (elements[j].type.block)
					{
						racers[i].position = elements[j].position - elements[j].lenght;
						racers[i].speed = 0;
		}
					else if (elements[j].type.boost)
					{
						racers[i].speed = RACE_SPEED_MAX;
	}
				}
			}
		}
	
		// Handle debouncing the lane buttons
		if (lane_debounce[i])
		{
			lane_debounce[i]--;
		}

	}

	
	if (finished == active_racers)
	{
		finished = 0;
		podium_cnt = 1;
		next_stage = STAGE_FINISHED;
	} else {
		// Build lane LUT
		for(uint8_t i = 0; i < RACE_PLAYERS; i++) {
			if(racers[i].active) {
				uint16_t position = racers[i].position;
				for (uint8_t j = 0; j < racers[i].lenght; j++)
				{
					if (position > TRACK_LANE_LENGTH_LEDS) // Wrap around
					{
						position += TRACK_LANE_LENGTH_LEDS;
					}
					led_strip[racers[i].lane][position--].byte |= (1 << i);
				}
			}
		}
	}
	
	return next_stage;
}

uint8_t finish_handler(void)
{
	static volatile uint16_t finish_timer = RACE_FINISH_COUNTDOWN;

	//Draw podium
	for (uint8_t i = 0; i < RACE_PLAYERS; i++)
	{
		if (racers[i].podium == 1)
		{
			for (uint8_t j = 0; j < 3; j++)
			{
				led_strip[j][10].byte |= (1 << i);
				led_strip[j][11].byte |= (1 << i);
				led_strip[j][12].byte |= (1 << i);
			}
		}
		else if (racers[i].podium == 2)
		{
			for (uint8_t j = 0; j < 2; j++)
			{
				led_strip[j][6].byte |= (1 << i);
				led_strip[j][7].byte |= (1 << i);
				led_strip[j][8].byte |= (1 << i);
			}
		}
		else if (racers[i].podium == 3)
		{
			led_strip[0][14].byte |= (1 << i);
			led_strip[0][15].byte |= (1 << i);
			led_strip[0][16].byte |= (1 << i);
		}
	}

	if (!finish_timer--)
	{
		// LEDs cleared in reset

		finish_timer = RACE_FINISH_COUNTDOWN;
		return STAGE_RESET;
	}
	
	return STAGE_FINISHED;
}

void dotstar_x3_write_start(void) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES; Byte_Count > 0; Byte_Count--) {
		dotstar0_send_byte(0x00);
		dotstar1_send_byte(0x00);
		dotstar2_send_byte(0x00);
	}
}

void dotstar_x3_write_end(uint16_t total_length) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES+(total_length/16)+1; Byte_Count > 0; Byte_Count--) {
		dotstar0_send_byte(0x00);
		dotstar1_send_byte(0x00);
		dotstar2_send_byte(0x00);
	}

	while(!(LED2_USART.STATUS & USART_TXCIF_bm));
	LED2_USART.STATUS = USART_TXCIF_bm;
	while(!(LED1_USART.STATUS & USART_TXCIF_bm));
	LED1_USART.STATUS = USART_TXCIF_bm;
	while(!(LED0_USART.STATUS & USART_TXCIF_bm));
	LED0_USART.STATUS = USART_TXCIF_bm;
}

void dotstar0_write_pixel_color(element_types_t type)
{
	static volatile uint8_t check_order = 0;

	// Colour based on bit set. In order.
	if (type.byte == 0)
	{
		dotstar0_write_single_rgb(BLACK);
	}
	else if (type.players) {
		// Rotate which player is checked first, to get overlap
		check_order++;
		for (uint8_t j = 0; j < RACE_PLAYERS; j++)
		{
			uint8_t player_bp = (check_order + j) % RACE_PLAYERS;
			if (type.players & (1 << player_bp))
			{
				if (player_bp == 0)
				{
					dotstar0_write_single_rgb(PLAYER1_COLOR);
				}
				else if (player_bp == 1)
				{
					dotstar0_write_single_rgb(PLAYER2_COLOR);
				}
				else // if (player_bp == 2)
				{
					dotstar0_write_single_rgb(PLAYER3_COLOR);
				}
				return; // Only first hit gets a color
			}
		}
	}
	else if (type.block)
	{
		dotstar0_write_single_rgb(BLOCK_COLOR);
	}
	else if (type.boost)
	{
		dotstar0_write_single_rgb(BOOST_COLOR);
	}
	else if (type.start)
	{
		dotstar0_write_single_rgb(RED_MAX);
	}
	else
	{
		dotstar0_write_single_rgb(BLACK);
	}
}

void dotstar1_write_pixel_color(element_types_t type)
{
	static volatile uint8_t check_order = 0;

	// Colour based on bit set. In order.
	if (type.byte == 0)
	{
		dotstar1_write_single_rgb(BLACK);
	}
	else if (type.players) {
		// Rotate which player is checked first, to get overlap
		check_order++;
		for (uint8_t j = 0; j < RACE_PLAYERS; j++)
		{
			uint8_t player_bp = (check_order + j) % RACE_PLAYERS;
			if (type.players & (1 << player_bp))
			{
				if (player_bp == 0)
				{
					dotstar1_write_single_rgb(PLAYER1_COLOR);
				}
				else if (player_bp == 1)
				{
					dotstar1_write_single_rgb(PLAYER2_COLOR);
				}
				else // if (player_bp == 2)
				{
					dotstar1_write_single_rgb(PLAYER3_COLOR);
				}
				return; // Only first hit gets a color
			}
		}
	}
	else if (type.block)
	{
		dotstar1_write_single_rgb(BLOCK_COLOR);
	}
	else if (type.boost)
	{
		dotstar1_write_single_rgb(BOOST_COLOR);
	}
	else if (type.start)
	{
		dotstar1_write_single_rgb(RED_MAX);
	}
	else
	{
		dotstar1_write_single_rgb(BLACK);
	}
}

void dotstar2_write_pixel_color(element_types_t type)
{
	static volatile uint8_t check_order = 0;

	// Colour based on bit set. In order.
	if (type.byte == 0)
	{
		dotstar2_write_single_rgb(BLACK);
	}
	else if (type.players) {
		// Rotate which player is checked first, to get overlap
		check_order++;
		for (uint8_t j = 0; j < RACE_PLAYERS; j++)
		{
			uint8_t player_bp = (check_order + j) % RACE_PLAYERS;
			if (type.players & (1 << player_bp))
			{
				if (player_bp == 0)
				{
					dotstar2_write_single_rgb(PLAYER1_COLOR);
				}
				else if (player_bp == 1)
				{
					dotstar2_write_single_rgb(PLAYER2_COLOR);
				}
				else // if (player_bp == 2)
				{
					dotstar2_write_single_rgb(PLAYER3_COLOR);
				}
				return; // Only first hit gets a color
			}
		}
	}
	else if (type.block)
	{
		dotstar2_write_single_rgb(BLOCK_COLOR);
	}
	else if (type.boost)
	{
		dotstar2_write_single_rgb(BOOST_COLOR);
	}
	else if (type.start)
	{
		dotstar2_write_single_rgb(RED_MAX);
	}
	else
	{
		dotstar2_write_single_rgb(BLACK);
	}
}


void dotstar_display_race(void) {
	dotstar_x3_write_start();
	for (uint16_t i = 0; i < TRACK_LANE_LENGTH_LEDS; i++)
	{
		// Colour based on bit set. In order.
		dotstar0_write_pixel_color(led_strip[0][i]);
		led_strip[0][i].players = 0;
		dotstar1_write_pixel_color(led_strip[1][i]);
		led_strip[1][i].players = 0;
		dotstar2_write_pixel_color(led_strip[2][i]);
		led_strip[2][i].players = 0;
	}
	dotstar_x3_write_end(TRACK_LANE_LENGTH_LEDS);
}

ISR(RTC_CNT_vect)   //Just for wakeup
{
	refresh = true;
	
	RTC.INTFLAGS = RTC_OVF_bm;
}

ISR(PLAYER1_vect) {
	handle_speed_isr(0);

	PLAYER1PORT.INTFLAGS = PLAYER1_bm;
}

#ifdef BUTTONS_XPRO // Testing with three buttons on oled1_xpro

ISR(PLAYER2_vect) { //== PLAYER3_vect
	if (PLAYER2PORT.INTFLAGS & PLAYER2_bm)
	{
		handle_speed_isr(1);
		PLAYER2PORT.INTFLAGS = PLAYER2_bm;
	}

	if (PLAYER3PORT.INTFLAGS & PLAYER3_bm)
	{
		handle_speed_isr(2);
		PLAYER2PORT.INTFLAGS = PLAYER3_bm;
	}
}

#else

ISR(PLAYER2_vect) {
	handle_speed_isr(1);	

	PLAYER2PORT.INTFLAGS = PLAYER2_bm;
}

ISR(PLAYER3_vect) {
	handle_speed_isr(2);
	
	PLAYER3PORT.INTFLAGS = PLAYER3_bm;
}

ISR(LANE_vect) {
	if (LANEPORT.INTFLAGS & PLAYER1LANE_bm)
	{
		handle_lane_isr(0);
		LANEPORT.INTFLAGS = PLAYER1LANE_bm;
	}
	
	if (LANEPORT.INTFLAGS & PLAYER2LANE_bm)
	{
		handle_lane_isr(1);
		LANEPORT.INTFLAGS = PLAYER2LANE_bm;
	}
	
	if (LANEPORT.INTFLAGS & PLAYER3LANE_bm)
	{
		handle_lane_isr(2);
		LANEPORT.INTFLAGS = PLAYER3LANE_bm;
	}
}

#endif

void handle_speed_isr(uint8_t button) {
	switch (game_stage)
	{
		case STAGE_RACE:
		if (racers[button].active)
		{
			racers[button].speed += RACE_SPEED_INCREMENT;
		}
		break;
		case STAGE_STOPPED:
		game_stage = STAGE_INIT;
		case STAGE_INIT:
		racers[button].active = true;
		case STAGE_COUNTDOWN:
		case STAGE_FINISHED:
		case STAGE_RESET:
		default:
		break;
	}
}

void handle_lane_isr(uint8_t button) {
	switch (game_stage)
	{
		case STAGE_RACE:
		if (racers[button].active && !lane_debounce[button])
		{
			//TODO: Move this into race_handler and replace with i-want-to-change-lane
			racers[button].speed += 1; // In case stopped
			if (++racers[button].lane >= TRACK_LANES)
			{
				racers[button].lane = 0;
			}
			lane_debounce[button] = RACE_LANE_DEBOUNCE;
		}
// 		break;
// 		case STAGE_STOPPED:
// 		case STAGE_INIT:
// 		case STAGE_COUNTDOWN:
// 		case STAGE_FINISHED:
// 		case STAGE_RESET:
		default:
		break;
	}
}