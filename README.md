# DotStar LED Racing

## Inspired by the Open LED Racing project (https://github.com/gbarbarov/led-race)
Made from scratch on an ATmega4809 and expanded to be 3-player with 3 parallel lanes and randomly placed block- and boost-fields.

## Hardware
* 9x 5m, 60 LEDs/m Dotstar LED strips
* 6x Tactile Switches
* 1x ATmega4809 Curiosity Nano (CNANO)
* 3x 5V/8A power supplies
* Quite a lot of wires

## Connections
* Connect the power wires on 3 LED strips in parallel with to a 5V power supply
  * Make sure the data direction of the 3 LED strips is aligned
* Connect the 3 parallel LED strips in series to get 3x 15m parallel LED strips
  * If you did it right you will now have one power supply at the start and between each 5m section
* Connect 5V to the VBUS pin on the ATmega4809 CNANO
  * Alternatively connect an USB cable to the kit
* Connect each 3x15m LED strip lane to the ATmega4809 CNANO:
  * Lane 1 DI -> PA4
  * Lane 1 CI  -> PA6
  * Lane 1 GND  -> GND
  * Lane 2 DI -> PC4
  * Lane 2 CI  -> PC6
  * Lane 2 GND  -> GND
  * Lane 3 DI -> PF0
  * Lane 3 CI  -> PF2
  * Lane 3 GND  -> GND
* Connect one pin on each button GND and the other to the ATmega4809 CNANO:
  * Player 1 Speed -> PA2
  * Player 2 Speed -> PE2
  * Player 3 Speed -> PC2
  * Player 1 Lane Change -> PD3
  * Player 2 Lane Change -> PD4
  * Player 3 Lane Change -> PD5

## Configuration
* Configure the CNANO to supply ~5V to the ATmega4809
* Power 
