/*
 * dotstar.c
 *
 * Created: 01.09.2018 00:40:57
 * Author: Egil
 */

#include <avr/io.h>
#include <stdint.h>
#include "dotstar.h"

/*
 * Local defines
 */


/*
 * Private functions
 */


/*
 * Public functions
 */

void dotstar0_init(void)
{
	LED0_USART.CTRLB = USART_TXEN_bm;
	LED0_USART.CTRLC = USART_CMODE_MSPI_gc;
//	LED0_USART.BAUD = USART_BAUD_RATE(5000000);
	LED0_USART.BAUD = 64*2; //64 is minimum, but that did not work for 900 LEDs

#if (LED0_DATA_PIN == PIN4_bm) //Alternate
	PORTMUX.USARTROUTEA |= LED0_ALT1;
#endif

	// Set MOSI and SCK to output
	LED0_USART_PORT.OUTCLR = LED0_DATA_PIN | LED0_SCK_PIN;
	LED0_USART_PORT.DIRSET = LED0_DATA_PIN | LED0_SCK_PIN;
}

void dotstar0_send_byte(uint8_t Data)
{
	while(!(LED0_USART.STATUS & USART_DREIF_bm)) {
		// Wait for empty buffer
	}

	LED0_USART.TXDATAL = Data;
}

void dotstar0_write_start(void) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES; Byte_Count > 0; Byte_Count--) {
		dotstar0_send_byte(0x00);
	}	
}

void dotstar0_write_end(uint16_t total_length) {
	for(uint8_t Byte_Count = DOTSTAR_LED_BYTES+(total_length/16)+1; Byte_Count > 0; Byte_Count--) {
		dotstar0_send_byte(0x00);
	}

	while(!(LED0_USART.STATUS & USART_TXCIF_bm));
	LED0_USART.STATUS = USART_TXCIF_bm;
}

void dotstar0_write_single_rgb(color_t Color)
{
	for(uint8_t Byte_Count = 0; Byte_Count < DOTSTAR_LED_BYTES; Byte_Count++) {
		dotstar0_send_byte(Color.array[Byte_Count]);
	}
}

void dotstar0_write_array_rgb(color_t *array, uint16_t length)
{
	uint8_t *LED_Bytes = (uint8_t *) array;
	for(uint16_t Byte_Count = 0; Byte_Count < length*DOTSTAR_LED_BYTES; Byte_Count++) {
		dotstar0_send_byte(LED_Bytes[Byte_Count]);
	}
}

void dotstar0_write_constant_rgb(color_t Color, uint16_t length)
{
	while(length--) {
		dotstar0_write_single_rgb(Color);
	}
}

void dotstar0_configure_single_rgb(color_t Color)
{
	dotstar0_write_start();
	dotstar0_write_single_rgb(Color);
	dotstar0_write_end(0);
}

void dotstar0_configure_array_rgb(color_t *LED_String, uint16_t LED_string_length)
{
	dotstar0_write_start();
	dotstar0_write_array_rgb(LED_String, LED_string_length);
	dotstar0_write_end(LED_string_length);
}

void dotstar0_configure_constant_rgb(color_t Color, uint16_t LED_string_length)
{
	dotstar0_write_start();
	dotstar0_write_constant_rgb(Color, LED_string_length);
	dotstar0_write_end(LED_string_length);
}

void dotstar0_configure_constant_and_single(color_t single, uint16_t single_position, color_t constant, uint16_t LED_string_length) {
	dotstar0_write_start();
	
	dotstar0_write_constant_rgb(constant, single_position);
	dotstar0_write_single_rgb(single);
	dotstar0_write_constant_rgb(constant, LED_string_length-single_position);

	dotstar0_write_end(LED_string_length);
}

void dotstar0_configure_pong(color_t *endzone1, color_t *endzone2, uint8_t endsize, color_t pong_color, uint8_t pong_position, uint8_t total_size) {
	color_t off = {.brightness = 0x1f};
	
	dotstar0_write_start();
	dotstar0_write_array_rgb(endzone1, endsize); // first 10 LEDs from array
	
	// middle LEDs either all black or with the pong somewhere
	if ((pong_position < endsize) || (pong_position >= total_size-endsize))
	{
		dotstar0_write_constant_rgb(off, total_size-(endsize*2));
	} else {
		dotstar0_write_constant_rgb(off, pong_position-endsize);
		dotstar0_write_single_rgb(pong_color);
		dotstar0_write_constant_rgb(off, total_size-endsize-pong_position-1);		
	}
	
	dotstar0_write_array_rgb(endzone2, endsize); // last 10 LEDs from array
	dotstar0_write_end(total_size);	
}